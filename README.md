# Terraform S3 backend module

Terraform [S3 backend](https://www.terraform.io/docs/backends/types/s3.html). S3 bucket for storage and DynamoDB table for locking.

## Features

* Unique S3 bucket name (random suffix)
* S3 bucket with versioning and AES256 Server Side Encryption
* Region taken from AWS provider

## Usage

    module "backend" {
        source        = "git::https://bitbucket.org/terraforming/terraform-aws-s3-backend?ref=master"
        deployment_id = "SOMETHING_UNIQUE"
    }

## Outputs

| Name | Description |
|------|-------------|
| bucket | S3 bucket for state storage |
| dynamodb_table | DynamoDB table for state locking |
| region | AWS region with S3 backend resources |

## Variables

| Name | Default | Description |
|------|---------|-------------|
| deployment_id | | Module deployment ID (may be generated with [terraform-deployment-id](https://bitbucket.org/terraforming/terraform-deployment-id) module) |

## Tips

### Backend management

You may use the Terraform [Workspaces](https://www.terraform.io/docs/state/workspaces.html) if you need to maintain backends in a several accounts/regions.

#### Create a new backend

Creating new backend should the very first thing in fresh AWS account setup after IAM roles (or users).

1. Configure the AWS credentials for an account where the backend should be setup

        aws configure --profile NEW

2. Create a Terraform file with the following content

        provider "aws" {}

        module "deployment" {
                source = "git::https://bitbucket.org/terraforming/terraform-deployment-id.git?ref=master"
        }

        module "backend" {
                source        = "git::https://bitbucket.org/terraforming/terraform-aws-s3-backend.git?ref=master"
                deployment_id = "${module.deployment.id}"
        }

        output "bucket" {
                value = "${module.backend.bucket}"
        }

        output "dynamodb_table" {
                value = "${module.backend.dynamodb_table}"
        }

        output "region" {
                value = "${module.backend.region}"
        }

3. Create new Workspace (best with the same name as a profile from above) and ensure it is selected

        terraform workspace new NEW
        terraform workspace show

4. Initialize the working directory

        terraform init

5. Check the plan and apply changes

        terraform plan
        terraform apply

    The backend will be created in region taken from AWS provider.

6. Commit the Terraform state file to repository

        git add terraform.tfstate.d/NEW/terraform.tfstate
        git commit -m "Terraform S3 backend in NEW"

    **Note:** it is not a best practice to commit Terraform state to a repository,
    but the backend state is an exception. It cannot be stored in backend because it did not exist in a time 
    when the state was created (chicken-egg problem).

    You may add the following into ```.gitignore```

        # Do not ignore the .tfstate from workspaces
        !terraform.tfstate.d/*/*.tfstate

#### Modify a backend configuration

1. Make the desired changes

2. **SWITCH TO CORRECT WORKSPACE**

        terraform workspace select NEW

3. Check the plan and apply changes

        terraform plan
        terraform apply

4. Commit the changes in appropriate _terraform.tfstate_ file

        git add terraform.tfstate.d/NEW/terraform.tfstate
        git commit -m "Terraform S3 backend in NEW updated"

#### Delete a backend

1. **SWITCH TO CORRECT WORKSPACE**

        terraform workspace select NEW

2. Delete everything (all versions) from S3 bucket in AWS Console

3. Destroy the resources

        terraform destroy

4. Delete the _terraform.tfstate_ from repository

        git rm -f terraform.tfstate.d/NEW/terraform.tfstate
        git commit -m "Terraform S3 backend in NEW deleted"

5. Delete Terraform Workspace

        terraform workspace select default
        terraform workspace delete NEW

### Write the Backend and Remote State config files

Generate the Backend and Remote State config files from deployment outputs.

        resource "local_file" "backend_tfvars" {
                filename = "${path.module}/backend.tfvars"

                content = <<EOF
        bucket         = "${module.backend.bucket}"
        dynamodb_table = "${module.backend.dynamodb_table}"
        region         = "${module.backend.region}"
        EOF
        }

        resource "local_file" "remote_state_tfvars" {
                filename = "${path.module}/remote-state.auto.tfvars"

                content = <<EOF
        backend_bucket         = "${module.backend.bucket}"
        backend_dynamodb_table = "${module.backend.dynamodb_table}"
        backend_region         = "${module.backend.region}"
        EOF
        }

Use the ```backend.tfvars``` in [init](https://www.terraform.io/docs/commands/init.html#backend-initialization) command for [partial backend configuration](https://www.terraform.io/docs/backends/config.html#partial-configuration).

        terraform init -backend-config=../aws-dev-backend/backend.tfvars

Copy the ```remote-state.auto.tfvars``` to directory where you would like to use [terraform_remote_state](https://www.terraform.io/docs/providers/terraform/d/remote_state.html) and use interpolation.

        variable "backend_bucket" {}
        variable "backend_dynamodb_table" {}
        variable "backend_region" {}

        data "terraform_remote_state" "vpc" {
                backend = "s3"

                config {
                        bucket         = "${var.backend_bucket}"
                        key            = "vpc.tfstate"
                        dynamodb_table = "${var.backend_dynamodb_table}"
                        region         = "${var.backend_region}"
                }
        }

### Storing state in the backend

### Strategy

#### Permissions

### Remote state

### Account setup
