data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

resource "aws_s3_bucket" "bucket" {
  bucket = "terraform-state-storage-${data.aws_region.current.name}-${var.deployment_id}"

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags {
    Name          = "Terraform S3 backend Storage"
    deployment_id = "${var.deployment_id}"
  }
}

resource "aws_dynamodb_table" "dynamodb_table" {
  name           = "terraform-state-lock-${data.aws_region.current.name}-${var.deployment_id}"
  hash_key       = "LockID"
  read_capacity  = 1
  write_capacity = 1

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name          = "Terraform S3 backend Lock"
    deployment_id = "${var.deployment_id}"
  }
}
