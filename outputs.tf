output "bucket" {
  description = "S3 bucket for state storage"
  value       = "${aws_s3_bucket.bucket.id}"
}

output "dynamodb_table" {
  description = "DynamoDB table for state locking"
  value       = "${aws_dynamodb_table.dynamodb_table.id}"
}

output "region" {
  description = "AWS region with S3 backend resources"
  value       = "${data.aws_region.current.name}"
}
